const { urlencoded } = require("express");
const express = require("express");
const sendEmail = require("./utils/nodemailer");
const cors = require("cors");
const fs = require("fs/promises");
const path = require("path");
const ejs = require("ejs");

const app = express();
app.use(cors());
app.use(express.json());
app.use(urlencoded({extended: true}));

app.post("/enviar-correo", async(req, res, next) => {
    try{
        const {subject, to} = req.body;
        const pathTemplate = path.resolve("src", "views/email_templates", "bienvenida.ejs");
        const template = await ejs.renderFile(pathTemplate, {title: "Hola mundo"});
        const options = {
            subject,
            html: template,
            to,
            from: "Academlo <oislasreyes@gmail.com>"
        };
    
        const response = await sendEmail(options);
    
        res.json(response);
    }catch(error){
        console.log(error);
    }   
});

app.post("/restablecer-contrasena", async(req, res, next) => {
    try{
        const subject = "Restablecimiento de contraseña";
        const to = ""; //Obtener el correo electronico del cliente que está restableciendo la contraseña
        const firstname = ""; //Obtener el nombre y apellido de la base de datos usando Sequelize
        const lastname = "";
        const pathTemplate = path.resolve("src", "views/email_templates", "bienvenida.ejs");
        const template = await ejs.renderFile(pathTemplate, {firstname: "Eduardo", lastname: "Hernandez"});
        const options = {
            subject,
            html: template,
            to,
            from: "Academlo <oislasreyes@gmail.com>"
        };
    
        const response = await sendEmail(options);
    
        res.json(response);
    }catch(error){
        console.log(error);
    }   
});

module.exports = app;